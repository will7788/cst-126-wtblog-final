<?php
/*CST-126 Completed Project, William Thornton, Version 1.0, 07/07/2019*/
    
    session_start();
    require('myFuncs.php');
    $con = dbConnect();

    //select first name and last name only if both username AND password match.
    $sql = "SELECT post_id, post_title, post_content, posted_by, deleted_flag, category_id, post_rating FROM posts";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($post_id, $post_title, $post_content, $posted_by, $deleted_flag, $category_id, $post_rating);
    $rows = $stmt->num_rows;
    if ($rows > 0){
        echo "<h2>Total number of posts: ".$rows."</h2>";
        while($stmt->fetch()){
            if ($deleted_flag == 'n')
            {
                echo '<div class="signup" style="margin-left:5%; width:90%; margin-top:1rem;">';
                echo '<h1 style="text-align:center;">'.$post_title.'</h1>';
                echo "<h2>Post rating: ".$post_rating."</h2>";
                echo "<p>Posted by user with ID: ".$posted_by."</p>";
                echo "<p>".$post_content."</p>";
                echo "<p>Post Category ID: ".$category_id."</p>";
                echo '<form action="comment.php" method="post" name="input" class="">
                        <input type="hidden" name="commentbtn" value="'.$post_id.'">
                        <input class="btn btn-dark btn-lg submission" type="submit" 
                        name="submit" value="Comment/Rate" style="margin-left:0;">
                        </form>';
                if ($_SESSION['userrole'] == 1 || $_SESSION['id'] == $posted_by)
                {
                    echo '<form action="removePost.php" method="post" name="input" class="">
                        <input type="hidden" name="removebtn" value="'.$post_id.'">
                        <input class="btn btn-dark btn-lg submission" type="submit" 
                        name="submit" value="Remove" style="margin-left:0;">
                        </form>';
                    echo '<form action="updatePost.php" method="post" name="input" class="">
                        <input type="hidden" name="update" value="'.$post_id.'">
                        <input class="btn btn-dark btn-lg submission" type="submit" 
                        name="submit" value="Update" style="margin-left:0;">
                        </form>';
                    echo '<form action="changeCategory.php" method="post" name="input">
                        <select class="selectpicker" name="category">
                             <option value="1">1</option>
                             <option value="2">2</option>
                             <option value="3">3</option>
                         </select>
                        <input type="hidden" name="removebtn" value="'.$post_id.'">
                        <input class="btn btn-dark btn-lg submission" type="submit" 
                        name="submit" value="Change Post Category" style="margin-left:0;">
                        </form>';
                }
                //Grab comments from comment table where post id matches
                $sql2 = "SELECT comment_id, comment_text, posted_by, deleted_flag FROM comments WHERE post_id = '$post_id'";
                $stmt2 = $con->prepare($sql2);
                $stmt2->execute();
                $stmt2->store_result();
                $stmt2->bind_result($comment_id, $comment_text, $cposted_by, $cdeleted_flag);
                $rows2 = $stmt2->num_rows;
                if($rows2 > 0)
                {
                    echo "<h2>Comments: </h2>";
                    while ($stmt2->fetch())
                    {
                        if($cdeleted_flag == 'n')
                        {
                            echo '<div class="signup" style="margin-left:5%; width:90%; margin-top:1rem;">';
                            echo "<p>Posted by user with ID: ".$cposted_by."</p>";
                            echo "<p>".$comment_text."</p>";
                            echo "</div>";
                        }
                    }
                }

                echo '</div>';
            }
        }


    }
    elseif($rows == 0){
        echo "<p>There are no posts</p>";
    }
    else{
        die('Connect Error:' . $con->connect_error);
    }

    $con->close();

?>