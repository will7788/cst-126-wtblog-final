<?php
/*CST-126 Completed Project, William Thornton, Version 1.0, 07/07/2019*/
    session_start();
    require('myFuncs.php');
    $con = dbConnect();

    $postid = $_POST[removebtn];
    
    $sql = "UPDATE posts SET deleted_flag = 'y' WHERE post_id = ?;";
    $stmt = $con->prepare($sql);
    $stmt->bind_param('s', $postid);
    $stmt->execute();
    $con->close();
    header("refresh:1; url=posts.php")

?>