<?php
/*CST-126 Completed Project, William Thornton, Version 1.0, 07/07/2019*/
?>

<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Search Posts | WTBlog</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC|Exo|Raleway" rel="stylesheet">
    
    <!-- SCRIPTS -->

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
    
        <nav class="navbar navbar-expand-lg navbar-dark nav">

            <a class="navbar-brand" href=""> <span class="nav-font">WTB</span><span class="small">log</span> </a>
        
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02">
                <span class="navbar-toggler-icon"></span>
            </button>
        
            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        
                <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="login.html">Log In</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="register.html">Sign Up</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="profile.php">My Account</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="posts.php">Posts</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="users.php">Users</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="searchPosts.php">Search Posts</a>
                </li>
                </ul>
        
            </div>
        </nav>
    <div class="signup" style="margin-left:15%; width:75%; margin-top:1rem;">
        <h4 style="text-align: center;">Search Results:</h4>

<?php
    
    session_start();
    require('myFuncs.php');
    $con = dbConnect();
    
    $searchQ = $_POST["search"];

    //select first name and last name only if both username AND password match.
    $sql = "SELECT post_id, post_title, post_content, posted_by, deleted_flag, category_id, post_rating FROM posts WHERE post_title LIKE '%".$searchQ."%' OR post_content LIKE '%".$searchQ."%'";
    $stmt = $con->prepare($sql);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($post_id, $post_title, $post_content, $posted_by, $deleted_flag, $category_id, $post_rating);
    $rows = $stmt->num_rows;
    if ($rows > 0){
        echo "<h2>Total number of posts: ".$rows."</h2>";
        while($stmt->fetch()){
            if ($deleted_flag == 'n')
            {
                echo '<div class="signup" style="margin-left:5%; width:90%; margin-top:1rem;">';
                echo '<h1 style="text-align:center;">'.$post_title.'</h1>';
                echo "<h2>Post rating: ".$post_rating."</h2>";
                echo "<p>Posted by user with ID: ".$posted_by."</p>";
                echo "<p>".$post_content."</p>";
                echo "<p>Post Category ID: ".$category_id."</p>";
                echo '<form action="comment.php" method="post" name="input" class="">
                        <input type="hidden" name="commentbtn" value="'.$post_id.'">
                        <input class="btn btn-dark btn-lg submission" type="submit" 
                        name="submit" value="Comment/Rate" style="margin-left:0;">
                        </form>';
                if ($_SESSION['userrole'] == 1 || $_SESSION['id'] == $posted_by)
                {
                    echo '<form action="removePost.php" method="post" name="input" class="">
                        <input type="hidden" name="removebtn" value="'.$post_id.'">
                        <input class="btn btn-dark btn-lg submission" type="submit" 
                        name="submit" value="Remove" style="margin-left:0;">
                        </form>';
                    echo '<form action="updatePost.php" method="post" name="input" class="">
                        <input type="hidden" name="update" value="'.$post_id.'">
                        <input class="btn btn-dark btn-lg submission" type="submit" 
                        name="submit" value="Update" style="margin-left:0;">
                        </form>';
                    echo '<form action="changeCategory.php" method="post" name="input">
                        <select class="selectpicker" name="category">
                             <option value="1">1</option>
                             <option value="2">2</option>
                             <option value="3">3</option>
                         </select>
                        <input type="hidden" name="removebtn" value="'.$post_id.'">
                        <input class="btn btn-dark btn-lg submission" type="submit" 
                        name="submit" value="Change Post Category" style="margin-left:0;">
                        </form>';
                }
                
                $sql2 = "SELECT comment_id, comment_text, posted_by, deleted_flag FROM comments WHERE post_id = '$post_id'";
                $stmt2 = $con->prepare($sql2);
                $stmt2->execute();
                $stmt2->store_result();
                $stmt2->bind_result($comment_id, $comment_text, $cposted_by, $cdeleted_flag);
                $rows2 = $stmt2->num_rows;
                if($rows2 > 0)
                {
                    echo "<h2>Comments: </h2>";
                    while ($stmt2->fetch())
                    {
                        if($cdeleted_flag == 'n')
                        {
                            echo '<div class="signup" style="margin-left:5%; width:90%; margin-top:1rem;">';
                            echo "<p>Posted by user with ID: ".$cposted_by."</p>";
                            echo "<p>".$comment_text."</p>";
                            echo "</div>";
                        }
                    }
                }
                echo '</div>';
            }
        }


    }
    elseif($rows == 0){
        echo "<p>There are no posts</p>";
    }
    else{
        die('Connect Error:' . $con->connect_error);
    }

    $con->close();

?>
    </div>

</body>
</html>